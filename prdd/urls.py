from django.urls import path

from .views import *

urlpatterns = [
    path('', Index.as_view()),
    path('token/<str:token>', from_link, name='token'),
    path('group', GroupChooseView.as_view(), name='group_choice'),
]
