from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
from django.db.models import QuerySet


class Department(models.Model):
    class Meta:
        verbose_name = "Département"

    short_name = models.CharField(max_length=5, verbose_name="Abréviation", unique=True)
    name = models.CharField(max_length=255, verbose_name="Nom")
    max_wave_seats = models.IntegerField(
        verbose_name="Nombre maximum de diplômés de ce département autorisés dans une vague",
        default=100)

    def __str__(self):
        return self.short_name

    @property
    def students_count(self) -> int:
        return self.students.all().count()


class Wave(models.Model):
    class Meta:
        verbose_name = "Vague"

    number = models.IntegerField(verbose_name="Numéro", unique=True)
    allowed_departments = models.ManyToManyField(to=Department, related_name='allowed_waves',
        verbose_name="Départements autorisés dans cette vague")

    def __str__(self):
        return f"Vague {self.number}"

    @property
    def students(self):
        # type: ()->QuerySet[Student]
        return Student.objects.filter(wave_group__in=self.groups.all())

    def used_seats_by_department(self, department: Department) -> int:
        """
        Permet de savoir le nombre d'élèves d'un département précis dans cette vague (tous groupes confondus)
        Il ne faut pas dépasser la moitité d'élèves d'un département dans chaque vague
        :param department:
        :return:
        """
        return self.students.filter(department=department).count()

    def free_seats_for_department(self, department: Department) -> int:
        if department not in self.allowed_departments.all():
            return 0
        else:
            return department.max_wave_seats - self.used_seats_by_department(department)

    def has_free_seats_for_department(self, department) -> bool:
        return self.free_seats_for_department(department) > 0 and department in self.allowed_departments.all() 

    @property
    def used_seats(self) -> int:
        return self.students.count()


class WaveGroup(models.Model):
    class Meta:
        verbose_name = "Groupe"

    wave = models.ForeignKey(to=Wave, null=False, related_name='groups', verbose_name="Vague", on_delete=models.CASCADE)
    number = models.IntegerField(
        verbose_name="Numéro")  # vérifier qu'il n'y a pas deux fois le même groupe pour une vague
    seats = models.IntegerField(verbose_name="Nombre de places", default=100)

    def __str__(self):
        return f"Groupe {self.number}  vague {self.wave.number}"

    @property
    def occupied_seats(self) -> int:
        get_user_model()
        return self.students.all().count()

    @property
    def free_seats(self) -> int:
        return self.seats - self.occupied_seats

    @property
    def has_free_seats(self) -> bool:
        return self.free_seats > 0


class Student(models.Model):
    class Meta:
        verbose_name = "Diplômé"

    token = models.CharField(max_length=32, verbose_name='Token invitation billevent', blank=False, null=False,
                             unique=True)
    first_name = models.CharField(max_length=4096, verbose_name="Prénom")
    last_name = models.CharField(max_length=4096, verbose_name="Nom")
    department = models.ForeignKey(to=Department, null=False, blank=False, related_name='students',
                                   on_delete=models.CASCADE, verbose_name="Département")
    wave_group = models.ForeignKey(to=WaveGroup, null=True, blank=True, related_name='students',
                                   on_delete=models.SET_NULL, verbose_name="Groupe")

    want_food_truck = models.CharField(choices=[
        ("oui","oui"),
        ("non","non"),
        ("-","pas répondu"),
    ],verbose_name="Prévoit d'aller manger aux food trucks", default="-", max_length=3)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def choose_group(self, group: WaveGroup) -> bool:
        """
        vérifie qu'il reste une place dans le groupe et qu'il n'y a pas trop de personnes de son département dans
        la vague
        :param group:
        :return:
        """
        if group.wave.has_free_seats_for_department(self.department) and group.has_free_seats:
            self.wave_group = group
            self.save()
            return True
        else:
            # afficher un détail de l'erreur
            return False  # raise ValidationError("Il n'y a plus de place dans ce groupe",code='invalid')
