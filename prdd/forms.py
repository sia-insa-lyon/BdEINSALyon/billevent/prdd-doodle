from django import forms

from prdd.models import Student, WaveGroup


def can_choose(group: WaveGroup, student: Student):
    return group.wave.has_free_seats_for_department(student.department) and group.has_free_seats


class StudentGroupChoiceForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('wave_group','want_food_truck')
