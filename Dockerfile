FROM python:3.6

EXPOSE 8000

RUN apt-get update && apt-get install -y libpq-dev python3-dev gcc g++
ENV LIBRARY_PATH=/lib:/usr/lib
RUN pip3 install psycopg2==2.8.6

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
#VOLUME /app/staticfiles
ENV DEBUG False
ENV HOSTS 127.0.0.2,127.0.0.3,127.0.0.1.nip.io
RUN chmod +x run-prod.sh
CMD /app/run-prod.sh
