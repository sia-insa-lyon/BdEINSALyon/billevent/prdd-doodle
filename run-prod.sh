#!/usr/bin/env sh
python3 manage.py migrate && python3 manage.py collectstatic --noinput && uwsgi --module=prddoodle.wsgi:application --http=0.0.0.0:8000 --threads=4 --processes=1