# Doodle PRDD
## Introduction
Cette application web permet aux diplômés de choisir leur groupe pour la Promenade de Remise Des Diplômes.

On définit deux vagues, et 4 groupes par vague.
Les diplômés choisissent leur groupe (et donc la vague associée) avec lequels ils se promèneront et 
assisteront à la cérémonie.

Il ne doit pas y avoir plus de la moitié des personnes (nombre configurable) d'un département
dans chaque vague, et il ne doit pas y avoir plus de 100 personnes dans un même groupe.

## Utilisation
### Préparation
Avec l'excel de la scolarité et la base de données du Gala, on importe dans notre base de données les éléments suivants:
 * nom/prénom (sert juste aux admins)
 * département
 * token du lien d'invitation billetterie (il n'a pas besoin d'être le même mais ça permettra de simplifier l'envoi des mails)

### Côté utilisateur
La personne clique sur son lien, ça la connecte, elle choisit son groupe et valide; et c'est fini !

### Côté Admin
Tout se passe sur l'admin Django. Il y a l'import/export Excel/csv... des choix des diplomés


# Doc développeur
```
git clone git@gitlab.com:sia-insa-lyon/BdEINSALyon/billevent/prdd-doodle.git prddoodle
cd prddoodle
python3 -m venv venv
source venv/bin/activate
./manage.py migrate
./manage.py runserver
```
crée avec python3.6 (pour la compat)